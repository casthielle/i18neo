# i18neo
[![npm version](https://img.shields.io/npm/v/i18neo.svg?label=i18neo&style=flat-square)](https://www.npmjs.com/package/i18neo)
[![license](https://img.shields.io/npm/l/i18neo.svg?style=flat-square)](https://www.npmjs.com/package/i18neo)
[![npm downloads](https://img.shields.io/npm/dt/i18neo.svg?style=flat-square)](https://www.npmjs.com/package/i18neo)

> Google CLI translator to generate i18n files.

- [Installation](#installation)
- [Options](#options)
- [Usage](#usage)
- [Contributing](#contributing)


## Installation

**Requirements**

- node >= 4.0.0

**With npm**

    $ sudo npm install i18neo -g

**With yarn**

    $ sudo yarn global add i18neo


## Options


|   Flag       |  alias  |  type              |  Description                                           |
|--------------|:-------:|:------------------:|:------------------------------------------------------:|
| --list       |   -l    |  null              |  List availables languages                             |
| --help       |   -h    |  null              |  Show help info                                        |
| --native     |   -n    |  string            |  Source language                                       |
| --idioms     |   -i    |  string            |  Languages to translate                                |
| --file       |   -f    |  file              |  File of texts to be translated                        |
| --text       |   -t    |  string            |  Text to be translated                                 |
| --output     |   -o    |  file - directory  |  Directory or file where the translations are stored   |


## Usage

### From terminal

> i18neo tries to automatically detect the language of what will be translated. If you wish, you can add the "--native" or "-n" mark to specify the language of the original text.

**Get simple text translation**

    $ i18neo -t "Hello word" -i es      // output: { text: "Hola mundo" }

**Get translation and spit in a file**

    $ i18neo -t "Hello word" -i es -o translations.json    // output: · output: translation.json

**Get translation and spit in a directory**

    $ i18neo -t "Hello word" -i es -o languages    // output: · output: languages

**Get translation from multiple languages ​​and spit in a directory**

    $ i18neo -t "Hello word" -i es,fr,uk,zh-HK -o languages    // output: · output: languages

**Get the translation of a file and spit in a directory**

    $ i18neo -f base.json -i es,fr -o languages    // output: · output: languages


## Contributing

1. Fork it ( https://gitlab.com/casthielle/i18neo/forks )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request