#!/usr/bin/env node

const argv = require('yargs')
.alias('l', 'list')
.alias('h', 'help')
.alias('n', 'native')
.alias('i', 'idioms')
.alias('f', 'file')
.alias('t', 'text')
.alias('o', 'output')
.help(false).argv;

const translator = require('../src/i18neo.js');

new translator(argv);