const config = {
    url: "https://translate.google.com/translate_a/single",
    headers: {
        "Charset": "UTF-8",
        "User-Agent": "AndroidTranslate/5.3.0.RC02.130475354-53000263 5.1 phone TRANSLATE_OPM5_TEST_1"
    },
    params: {
        client: 'at',
        dt: 't',
        dj: '1',
        ie: 'UTF-8',
        oe: 'UTF-8',
        iid: '1dd3b944-fa62-4b55-b330-74909a99969e'
    }
}

module.exports = config