const axios = require('axios');
const config = require('./config');
const fs = require('fs');

class i18n {

    constructor(argv) {
        this.from = 'auto';
        this.to = ['es'];
        this.input = null;
        this.output = null;
        this.type = null;
        this.isDir = null;
        this.data = [];

        if (argv.list)      this.list();
        if (argv.help)      this.info();
        if (argv.native)    this.from = argv.native;
        if (argv.idioms)    this.to = argv.idioms.split(",");
        if (argv.file)      this.type = "file";
        if (argv.text)      this.type = "text";
        if (argv.output)    this.output = argv.output;
        try { this.isDir = (this.output && fs.lstatSync(this.output).isDirectory()); }
        catch (e) { this.isDir = false; }

        this.input = (this.type === "text") ? { text: argv.text } : (argv.file ? require(`${process.cwd()}/${argv.file}`) : this.input );

        if (this.input) this.internationalize();
    }

    list(){ fs.readFile(`${__dirname}/languages.txt`, 'utf-8', (err, data) => { if (err) throw err; console.log(data); }); }
    info(){ fs.readFile(`${__dirname}/info.txt`, 'utf-8', (err, data) => { if (err) throw err; console.log(data); }); }

    make() {
        if (this.isDir) {
            this.data.forEach((translation => {
                let out = this.output.endsWith('/') ? this.output : this.output + '/';
                let file = `${out}${translation.file}`;
                let formatted = JSON.stringify(translation.translated, null, 2);
                fs.writeFile(file, formatted, (err) => { if (err) throw err });
            }));
            return;
        }

        let translated = {};
        this.data.forEach(item => { translated[item.language] = item.translated });
        let formatted = JSON.stringify(translated, null, 2);
        fs.writeFile(this.output, formatted, (err) => { if (err) throw err });
    }

    translate(from, to) {
        let translated = {};
        return new Promise(async (resolve) => {
            let text = Object.values(this.input).join('\n');
            let result = await this.google(from, to, text);
            let x = 0;
            for (let i in this.input) {
                translated[i] = result[x];
                x++;
            }
            resolve({ file: `${to}.i18n.properties.json`, translated });
        })
    }

    async internationalize() {
        for (let to of this.to) {
            let response = await this.translate(this.from, to);
            response.language = to;
            this.data.push(response);
        }

        if (this.output !== null) { this.make(); }

        let spit = (this.type === "text") ? this.data[0].translated : ` · output: ${this.output}`;

        console.log(spit);
    }

    async google(sl, tl, q){
        let { url, headers, params } = config;
        params = { ...params, sl, tl, q };
        let response = await axios.get(url, {
            headers: headers,
            params: params
        })
        
        let { sentences } = response.data;
        return sentences.map(sentence => sentence.trans.split('\n')[0]);
    }
}; 

module.exports = i18n;